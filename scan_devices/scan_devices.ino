#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>

const float n = 2.0;  // Path loss exponent
const float A = -60;  // Reference RSSI at 1 meter

int interval_time =2000;
 float dist =0;
 String mac_addr ="";

float calculateDistance(int rssi) {
  return pow(10, (A - rssi) / (10 * n));
}

void setup() {
  Serial.begin(115200);
  BLEDevice::init("ESP32");
}

void loop() {
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setActiveScan(true);
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);

  BLEScanResults foundDevices = pBLEScan->start(5, false);
  
  Serial.print("Number of devices found: ");
  
  
  Serial.println(foundDevices.getCount());

  for (int i = 0; i < foundDevices.getCount(); i++) {
    BLEAdvertisedDevice device = foundDevices.getDevice(i);


    mac_addr = device.getAddress().toString().c_str();

    if (mac_addr == "94:b9:7e:d9:6f:ae"){
      Serial.println("FOUND");
    Serial.print("Device ");
    Serial.print(i);
    Serial.print(": ");
//    Serial.print(device.getAddress().toString().c_str());
    Serial.print(mac_addr);
    Serial.print(", RSSI: ");
    Serial.print(device.getRSSI());
    Serial.print(", Distance: ");
    Serial.println(calculateDistance(device.getRSSI()));
     dist = calculateDistance(device.getRSSI());
    }

 
  }


  


//  if (dist < 400){
//    if (interval_time > 100){
//      interval_time = interval_time - 500;
//    }else{
//      Serial.println("minimum interval time reached");
//    }
//    
//    
//  }

  delay(2000);
}
